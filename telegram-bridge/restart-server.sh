#!/bin/bash

# Include ~/.profile because cron doesn't
source ~/.profile

# see if we can get an uid
TELEIRC_UID=$(forever list | grep "teleirc" | tr -s " " | cut -d " " -f3)
if [ -z $TELEIRC_UID ]; then
    echo "TeleIRC is not running"
    exit 1
fi

# change into irc-notifications
cd `dirname $0`/../

# check for updates on fetch, restart server if necessary
git fetch
if [[ $(git log HEAD..origin/master --oneline) ]]; then
    git pull -q
    forever restart $TELEIRC_UID
    echo "TeleIRC server restarted, prompted by change in irc-notifications repo"
fi
